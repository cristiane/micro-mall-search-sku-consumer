module gitee.com/cristiane/micro-mall-search-sku-consumer

go 1.13

require (
	gitee.com/kelvins-io/common v1.1.5
	gitee.com/kelvins-io/kelvins v1.6.3
	github.com/elastic/go-elasticsearch/v7 v7.9.0
	google.golang.org/grpc v1.40.0
)
