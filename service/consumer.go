package service

import (
	"context"
	"fmt"
	"gitee.com/cristiane/micro-mall-search-sku-consumer/model/args"
	"gitee.com/cristiane/micro-mall-search-sku-consumer/repository"
	"gitee.com/kelvins-io/common/errcode"
	"gitee.com/kelvins-io/common/json"
	"gitee.com/kelvins-io/kelvins"
)

func NoticeConsume(ctx context.Context, body string) error {
	var err error
	var msg args.CommonBusinessMsg
	err = json.Unmarshal(body, &msg)
	if err != nil {
		return err
	}
	if msg.Type != args.SkuInventorySearchNotice {
		return nil
	}
	var skuInventory args.SkuInventoryInfo
	err = json.Unmarshal(msg.Content, &skuInventory)
	if err != nil {
		return err
	}
	retCode := repository.SearchStore(ctx, args.ESIndexSkuInventory, skuInventory.SkuCode, msg.Content)
	if retCode == errcode.SUCCESS {
		return nil
	}
	err = fmt.Errorf(errcode.GetErrMsg(retCode))
	kelvins.BusinessLogger.Errorf(ctx, "skuInventory SearchStore err: %v, content: %v", errcode.GetErrMsg(retCode), msg)

	return err
}

func NoticeConsumeErr(ctx context.Context, err, body string) error {
	return nil
}
