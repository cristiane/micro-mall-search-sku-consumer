package startup

import (
	"gitee.com/cristiane/micro-mall-search-sku-consumer/vars"
	es7 "github.com/elastic/go-elasticsearch/v7"
	"log"
	"strings"
)

func SetupVars() error {
	var err error
	cfg := es7.Config{
		Addresses: strings.Split(vars.ElasticsearchSetting.Addresses, ","),
		Username:  vars.ElasticsearchSetting.Username,
		Password:  vars.ElasticsearchSetting.Password,
	}
	vars.ElasticsearchClient, err = es7.NewClient(cfg)
	if err != nil {
		log.Fatalf("Config elasticsearch err: %v", err)
	}
	res, err := vars.ElasticsearchClient.Info()
	if err != nil {
		log.Fatalf("Load elasticsearch info response err: %s", err)
	}
	defer res.Body.Close()
	if res.IsError() {
		log.Fatalf("Elasticsearch error: %s", res.String())
	}
	log.Printf("Elasticsearch info: %+v", res)
	log.Printf("Config elasticsearch ok")
	return err
}

func StopFunc() error {
	var err error

	return err
}
