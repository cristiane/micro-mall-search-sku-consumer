package startup

import (
	"gitee.com/cristiane/micro-mall-search-sku-consumer/vars"
	"gitee.com/kelvins-io/kelvins/config"
)

const (
	SectionEmailConfig         = "email-config"
	SectionElasticsearchConfig = "elasticsearch-config"
)

// LoadConfig 加载配置对象映射
func LoadConfig() error {
	// 邮箱
	vars.EmailConfigSetting = new(vars.EmailConfigSettingS)
	config.MapConfig(SectionEmailConfig, vars.EmailConfigSetting)
	// 加载ES
	vars.ElasticsearchSetting = new(vars.ElasticsearchSettingS)
	config.MapConfig(SectionElasticsearchConfig, vars.ElasticsearchSetting)
	return nil
}
