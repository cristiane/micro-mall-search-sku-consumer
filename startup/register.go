package startup

import "gitee.com/cristiane/micro-mall-search-sku-consumer/service"

const (
	SkuInventorySearchNoticeTag    = "sku_inventory_search_notice"
	SkuInventorySearchNoticeTagErr = "sku_inventory_search_notice_err"
)

func GetNamedTaskFuncs() map[string]interface{} {

	var taskRegister = map[string]interface{}{
		SkuInventorySearchNoticeTag:    service.NoticeConsume,
		SkuInventorySearchNoticeTagErr: service.NoticeConsumeErr,
	}
	return taskRegister
}
